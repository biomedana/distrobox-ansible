# Ansible files for distrobox boxes

## [Optional] Clone this repo

```bash
git clone https://gitlab.com/sebio/distrobox-ansible.git
cd distrobox-ansible
```


## Alpine

```bash
distrobox create --image alpine --name alpine

dropbox enter alpine

# run ansible inside the container
apk add ansible git 

# Option 1
ansible-pull -U https://gitlab.com/sebio/distrobox-ansible.git -i localhost, alpine.yml

# Option 2 locally
sudo ansible-playbook -i localhost, -c local alpine.yml
```


## Ubuntu

```bash
distrobox create --image docker.io/library/ubuntu:22.04 --name ubuntu
distrobox enter ubuntu

sudo apt install -y ansible 

# Option 1
ansible-pull -U https://gitlab.com/sebio/distrobox-ansible.git -i localhost, ubuntu.yml

# Option 2
sudo ansible-playbook -i localhost, -c local ubuntu.yml
```
